from flask import request, render_template, redirect, url_for
from flask_login import current_user, login_user, logout_user
from sqlalchemy import func, and_
from math import ceil
from datetime import datetime, timedelta

from mg52_webapp_flask import app, db
from mg52_webapp_flask.models import Items, Users, Basket, BonusCards, Addresses, Order, ItemsInOrder, Favorites, Feedbacks
from mg52_webapp_flask.forms import AddUserForm, LoginForm, AddItemForm, ImportItemForm, RegistrationForm, \
    AddToBasketForm, AddAddressForm, CreateOrderForm, EditUserForm, EditOrderForm, AddFeedbackForm

DEFAULT_ITEMS_ON_PAGE = 10
PAGINATION_SIZES = (2, 5, 10, 20, 100)
DAYS_DELIVERY_LAT = 5
PAY_CASH = 1
PAY_CARD = 2
ORDER_STATE_OPEN = 1
ORDER_STATE_DELIVERY = 2
ORDER_STATE_CLOSED = 3


def get_status_text(status):
    return 'ОТКРЫТ' if status == ORDER_STATE_OPEN else ('ЗАКРЫТ' if status == ORDER_STATE_CLOSED else 'ДОСТАВКА')


def get_pay_meth_text(pay_meth):
    return 'Наличными при получении' if pay_meth == PAY_CASH else 'Картой при получении'


def _get_page_items_and_count(request):
    try:
        items_on_page = int(request.args.get('page_items'))
        if items_on_page < 1:
            raise ValueError
    except (ValueError, TypeError):
        items_on_page = DEFAULT_ITEMS_ON_PAGE
    try:
        page_number = int(request.args.get('page'))
        if page_number < 1:
            raise ValueError
    except (ValueError, TypeError):
        page_number = 1
    return (items_on_page, page_number)


def _get_item_id_and_item(request, _table):
    try:
        item_id = int(request.args.get('item_id'))
        if item_id < 1:
            raise ValueError
    except (ValueError, TypeError):
        return (-1, 0)
    try:
        current_item = _table.query.filter(_table.id == item_id).all()[0]
    except IndexError:
        return (-2, 0)
    return (item_id, current_item)


def add_new_user(login, password, is_admin, firstname, lastname, email, phone, last_sess=datetime.now()):
    user = Users(login=login, is_admin=is_admin, firstname=firstname, lastname=lastname, email=email, phone=phone, last_sess=last_sess)
    user.set_pass(password)
    db.session.add(user)
    db.session.commit()
    db.session.add(BonusCards(card_number=str(user.id).zfill(20), user_id=user.id, balance=0, is_blocked=False))
    db.session.commit()


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    login_form = LoginForm()
    if login_form.validate_on_submit():
        user = Users.query.filter_by(login=login_form.login.data).first()
        if user is None or not user.check_pass(login_form.password.data):
            return render_template('login_template.html', form=login_form)
        login_user(user, remember=login_form.remember_me.data)
        # update time of last session
        user.last_sess = datetime.now()
        db.session.commit()
        return redirect(url_for('home'))
    return render_template('login_template.html', form=login_form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('home'))


@app.route('/')
@app.route('/home')
def home():
    return redirect(url_for('show_items'))


@app.route('/add_user', methods = ['GET', 'POST'])
def add_user():
    add_user_form = AddUserForm()
    if add_user_form.validate_on_submit():
        add_new_user(login=add_user_form.login.data,
                     password=add_user_form.password.data,
                     is_admin=add_user_form.is_admin.data,
                     firstname=add_user_form.firstname.data,
                     lastname=add_user_form.lastname.data,
                     email=add_user_form.email.data,
                     phone=add_user_form.phone.data)
        return render_template('msg_template.html',
                               title='user added', msg='User {} added successfully'.format(add_user_form.login.data))
    else:
        return render_template('add_user_form.html',
                               form=add_user_form,
                               title='Add user',
                               result_url=url_for('add_user'))


@app.route('/add_feedback', methods = ['GET', 'POST'])
def add_feedback():
    item_id, current_item = _get_item_id_and_item(request, Items)
    if item_id == -1:
        return render_template('msg_template.html',
                               title='bad item id', msg='id {} is not correct'.format(request.args.get('item_id')))
    elif item_id == -2:
        return render_template('msg_template.html',
                               title='bad item id',
                               msg='item  with id {} is not found'.format(request.args.get('item_id')))
    add_feedback_form = AddFeedbackForm()
    if add_feedback_form.validate_on_submit():
        db.session.add(Feedbacks(user_id=current_user.id,
                                 item_id=item_id,
                                 feedback=add_feedback_form.feedback.data))
        db.session.commit()
        return render_template('msg_template.html', title='feedback added', msg='Feedback added successfully')
    else:
        return render_template('add_feedback_form.html',
                               form=add_feedback_form,
                               result_url=url_for('add_feedback', item_id=item_id))


@app.route('/edit_feedback', methods = ['GET', 'POST'])
def edit_feedback():
    feed_id, current_feed = _get_item_id_and_item(request, Feedbacks)
    if feed_id == -1:
        return render_template('msg_template.html',
                               title='bad feed id', msg='id {} is not correct'.format(request.args.get('item_id')))
    elif feed_id == -2:
        return render_template('msg_template.html',
                               title='bad feed id',
                               msg='feed  with id {} is not found'.format(request.args.get('item_id')))
    edit_feedback_form = AddFeedbackForm()
    if edit_feedback_form.validate_on_submit():
        current_feed.feedback = edit_feedback_form.feedback.data
        db.session.commit()
        return render_template('msg_template.html', title='feedback edited', msg='Feedback edited successfully')
    else:
        edit_feedback_form.feedback.data = current_feed.feedback
        return render_template('add_feedback_form.html',
                               form=edit_feedback_form,
                               result_url=url_for('edit_feedback', item_id=feed_id))


@app.route('/delete_feedback')
def delete_feedback():
    feed_id, current_feed = _get_item_id_and_item(request, Feedbacks)
    if feed_id == -1:
        return render_template('msg_template.html',
                               title='bad feed id', msg='id {} is not correct'.format(request.args.get('item_id')))
    elif feed_id == -2:
        return render_template('msg_template.html',
                               title='bad feed id',
                               msg='feed with id {} is not found'.format(request.args.get('item_id')))
    db.session.delete(current_feed)
    db.session.commit()
    return render_template('msg_template.html',
                           title='feedback deleted', msg='Feedback deleted successfully')


@app.route('/feedbacks')
def feedbacks():
    items_on_page, page_number = _get_page_items_and_count(request)
    item_id, current_item = _get_item_id_and_item(request, Items)
    if item_id == -1:
        return render_template('msg_template.html',
                               title='bad item id', msg='id {} is not correct'.format(request.args.get('item_id')))
    elif item_id == -2:
        return render_template('msg_template.html',
                               title='bad item id',
                               msg='item  with id {} is not found'.format(request.args.get('item_id')))
    t_headers = [('user', 'width=20%'), ('feedback', None), ('edit', None), ('delete', None),]
    all_feeds = Feedbacks.query.filter(Feedbacks.item_id == item_id).limit(items_on_page).offset(items_on_page * (page_number - 1)).all()
    t_rows = []
    for feed in all_feeds:
        feed_user = Users.query.filter(Users.id == feed.user_id).all()
        if feed_user:
            feed_user = feed_user[0]
        can_manage_this_feed = not current_user.is_anonymous and (feed.user_id == current_user.id or current_user.is_admin)
        t_rows.append([{'icon': None, 'text': feed_user.firstname + ' ' + feed_user.lastname if feed_user else 'DELETED', 'url': None,},
                       {'icon': None, 'text': feed.feedback, 'url': None,},
                       {'icon': 'glyphicon glyphicon-cog' if can_manage_this_feed else None,
                        'text': None,
                        'url': url_for('edit_feedback', item_id=feed.id) if can_manage_this_feed else None},
                       {'icon': 'glyphicon glyphicon-remove' if can_manage_this_feed else None,
                        'text': None,
                        'url': url_for('delete_feedback', item_id=feed.id) if can_manage_this_feed else None}])
    return render_template('feedbacks.html',
                           pg_items_opts=[{'count': i, 'selected': True if i == items_on_page else False} for i in PAGINATION_SIZES],
                           pg_number=page_number,
                           count_of_pages=ceil(len(Feedbacks.query.filter(Feedbacks.item_id == item_id).all()) / items_on_page),
                           items_on_page=items_on_page,
                           item_id=item_id,
                           table_headers=t_headers,
                           table_rows=t_rows)


@app.route('/edit_user', methods = ['GET', 'POST'])
def edit_user():
    user_id, selected_user = _get_item_id_and_item(request, Users)
    if user_id == -1:
        return render_template('msg_template.html',
                               title='bad user id', msg='id {} is not correct'.format(request.args.get('item_id')))
    elif user_id == -2:
        return render_template('msg_template.html',
                               title='bad user id', msg='user  with id {} is not found'.format(request.args.get('item_id')))
    edit_user_form = EditUserForm()
    if edit_user_form.validate_on_submit():
        selected_user.is_admin = edit_user_form.is_admin.data
        selected_user.firstname = edit_user_form.firstname.data
        selected_user.lastname = edit_user_form.lastname.data
        selected_user.email = edit_user_form.email.data
        selected_user.phone = edit_user_form.phone.data
        db.session.commit()
        return render_template('msg_template.html',
                               title='user updated',
                               msg='user {} {} updated successfully'.format(selected_user.firstname,
                                                                               selected_user.lastname))
    else:
        edit_user_form.is_admin.data = selected_user.is_admin
        edit_user_form.firstname.data = selected_user.firstname
        edit_user_form.lastname.data = selected_user.lastname
        edit_user_form.email.data = selected_user.email
        edit_user_form.phone.data = selected_user.phone
        return render_template('add_user_form.html',
                               title='Edit user',
                               form=edit_user_form,
                               result_url=url_for('edit_user', item_id=user_id))


@app.route('/delete_user')
def delete_user():
    usr_id, current_usr = _get_item_id_and_item(request, Users)
    if usr_id == -1:
        return render_template('msg_template.html',
                               title='bad user id', msg='id {} is not correct'.format(request.args.get('item_id')))
    elif usr_id == -2:
        return render_template('msg_template.html',
                               title='bad use id', msg='user  with id {} is not found'.format(request.args.get('item_id')))
    # can not delete user if there are some count of orders for this user
    if Order.query.filter(Order.user_id == usr_id).all():
        return render_template('msg_template.html',
                               title='can not delete user',
                               msg='Can not delete user because there are some count of orders for this user')
    # delete bonus cards
    for b_card in BonusCards.query.filter(BonusCards.user_id == usr_id).all():
        db.session.delete(b_card)
    # delete favorites
    for fwr in Favorites.query.filter(Favorites.user_id == usr_id).all():
        db.session.delete(fwr)
    # delete addresses
    for adr in Addresses.query.filter(Addresses.user_id == usr_id).all():
        db.session.delete(adr)
    # delete basket
    for bck in Basket.query.filter(Basket.user_id == usr_id).all():
        db.session.delete(bck)
    db.session.delete(current_usr)
    db.session.commit()
    return render_template('msg_template.html',
                           title='user deleted', msg='User deleted successfully')


@app.route('/register', methods = ['GET', 'POST'])
def register():
    register_form = RegistrationForm()
    if register_form.validate_on_submit():
        add_new_user(login=register_form.login.data,
                     password=register_form.password.data,
                     is_admin=False,
                     firstname=register_form.firstname.data,
                     lastname=register_form.lastname.data,
                     email=register_form.email.data,
                     phone=register_form.phone.data)
        return redirect('/login')
    else:
        return render_template('register_template.html',
                               form=register_form)


@app.route('/personal')
def personal():
    bonus_card = BonusCards.query.filter(BonusCards.user_id == current_user.id).all()
    if len(bonus_card) == 0:  # bonus_card not found=(
        bonus_card = None
    else:
        bonus_card = bonus_card[0]
    items_on_page, page_number = _get_page_items_and_count(request)
    t_headers = [('country', 'width=10%'), ('region', 'width=20%'), ('city', 'width=20%'), ('street', 'width=20%'), ('house', 'width=10%'), ('apart', 'width=10%'),  ('update', 'width=5%'), ('delete', 'width=5%')]
    addresses = Addresses.query.filter(Addresses.user_id == current_user.id).limit(items_on_page).offset(items_on_page * (page_number - 1)).all()
    t_rows = [[{'icon': None, 'text': adr.country, 'url': None, },
               {'icon': None, 'text': adr.region, 'url': None, },
               {'icon': None, 'text': adr.city, 'url': None, },
               {'icon': None, 'text': adr.street, 'url': None, },
               {'icon': None, 'text': adr.house, 'url': None, },
               {'icon': None, 'text': adr.apart, 'url': None, },
               {'icon': 'glyphicon glyphicon-cog', 'text': None, 'url': url_for('edit_address', item_id=adr.id)},
               {'icon': 'glyphicon glyphicon-remove', 'text': None, 'url': url_for('delete_address', item_id=adr.id)}] for adr in addresses]
    return render_template('personal_page.html',
                           bonus_card=bonus_card,
                           pg_items_opts=[{'count': i, 'selected': True if i == items_on_page else False} for i in PAGINATION_SIZES],
                           pg_number=page_number,
                           count_of_pages=ceil(len(addresses) / items_on_page),
                           items_on_page=items_on_page,
                           title='Personal page',
                           table_headers=t_headers,
                           table_rows=t_rows)


@app.route('/add_address', methods = ['GET', 'POST'])
def add_address():
    add_adr_form = AddAddressForm()
    if add_adr_form.validate_on_submit():
        db.session.add(Addresses(country=add_adr_form.country.data,
                                 region=add_adr_form.region.data,
                                 city=add_adr_form.city.data,
                                 street=add_adr_form.street.data,
                                 house=add_adr_form.house.data,
                                 apart=add_adr_form.apart.data,
                                 user_id=current_user.id))
        db.session.commit()
        return render_template('msg_template.html',
                               title='address added',
                               msg='address {} {} {} {} {} {} added successfully'.format(add_adr_form.country.data,
                                                                                         add_adr_form.region.data,
                                                                                         add_adr_form.city.data,
                                                                                         add_adr_form.street.data,
                                                                                         add_adr_form.house.data,
                                                                                         add_adr_form.apart.data))
    else:
        return render_template('add_address_form.html',
                               title='Add address',
                               result_url=url_for('add_address'),
                               form=add_adr_form)


@app.route('/edit_address', methods = ['GET', 'POST'])
def edit_address():
    adr_id, current_adr = _get_item_id_and_item(request, Addresses)
    if adr_id == -1:
        return render_template('msg_template.html',
                               title='bad address id', msg='id {} is not correct'.format(request.args.get('item_id')))
    elif adr_id == -2:
        return render_template('msg_template.html',
                               title='bad address id', msg='address  with id {} is not found'.format(request.args.get('item_id')))
    edit_address_form = AddAddressForm()
    if edit_address_form.validate_on_submit():
        current_adr.country = edit_address_form.country.data
        current_adr.region = edit_address_form.region.data
        current_adr.city = edit_address_form.city.data
        current_adr.street = edit_address_form.street.data
        current_adr.house = edit_address_form.house.data
        current_adr.apart = edit_address_form.apart.data
        db.session.commit()
        return render_template('msg_template.html',
                               title='address updated',
                               msg='address {} {} {} {} {} {} updated successfully'.format(edit_address_form.country.data,
                                                                                        edit_address_form.region.data,
                                                                                        edit_address_form.city.data,
                                                                                        edit_address_form.street.data,
                                                                                        edit_address_form.house.data,
                                                                                        edit_address_form.apart.data))
    else:
        edit_address_form.country.data = current_adr.country
        edit_address_form.region.data = current_adr.region
        edit_address_form.city.data = current_adr.city
        edit_address_form.street.data = current_adr.street
        edit_address_form.house.data = current_adr.house
        edit_address_form.apart.data = current_adr.apart
        return render_template('add_address_form.html',
                               title='Edit address',
                               form=edit_address_form,
                               result_url=url_for('edit_address', item_id=adr_id))


@app.route('/create_order', methods = ['GET', 'POST'])
def create_order():
    create_order_form = CreateOrderForm()
    date_now = datetime.now()
    poss_delivery_dates = [date_now + timedelta(days=DAYS_DELIVERY_LAT+i) for i in range(5)]
    months = ('', 'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь')
    create_order_form.address.choices=[(str(adr.id), '{} {} {} {} {}'.format(adr.country, adr.region, adr.city, adr.street, adr.house))
                                       for adr in Addresses.query.filter(Addresses.user_id == current_user.id).all()]
    create_order_form.delivery_date.choices=[(str(i), months[d_d.month] + d_d.strftime(' %d')) for i, d_d in enumerate(poss_delivery_dates)]
    create_order_form.payment_method.choices = [(str(PAY_CASH), get_pay_meth_text(PAY_CASH)), (str(PAY_CARD), get_pay_meth_text(PAY_CARD))]
    if create_order_form.validate_on_submit():
        new_order = Order(user_id=current_user.id,
                          status=ORDER_STATE_OPEN,
                          assigned_to=2,  # TODO
                          created=date_now,
                          deadline=poss_delivery_dates[int(create_order_form.delivery_date.data)],
                          address_id=create_order_form.address.data,
                          delivery_cost=0, # TODO
                          comment=create_order_form.comment.data,
                          pay_meth=create_order_form.payment_method.data)
        db.session.add(new_order)
        db.session.commit()
        for item_ in Basket.query.filter(Basket.user_id == current_user.id).all():
            db.session.add(ItemsInOrder(order_id=new_order.id,
                                        item_id=item_.item_id,
                                        item_count=item_.item_count))
            db.session.delete(item_)
            db.session.commit()
        return render_template('msg_template.html',
                               title='Order placed', msg='Your order placed successfully')
    else:
        return render_template('create_order.html',
                               title='Create order',
                               form=create_order_form,
                               result_url=url_for('create_order'))


@app.route('/edit_order', methods = ['GET', 'POST'])
def edit_order():
    item_id, current_order = _get_item_id_and_item(request, Order)
    if item_id == -1:
        return render_template('msg_template.html',
                               title='bad order id', msg='id {} is not correct'.format(request.args.get('item_id')))
    elif item_id == -2:
        return render_template('msg_template.html',
                               title='bad order id', msg='order  with id {} is not found'.format(request.args.get('item_id')))
    edit_order_form = EditOrderForm()
    edit_order_form.status.choices=[(str(i), get_status_text(i)) for i in (ORDER_STATE_OPEN, ORDER_STATE_DELIVERY, ORDER_STATE_CLOSED)]
    if edit_order_form.validate_on_submit():
        current_order.status = edit_order_form.status.data
        db.session.commit()
        return render_template('msg_template.html', title='order updated', msg='Order updated successfully')
    else:
        edit_order_form.status.data = current_order.status
        return render_template('edit_order_form.html',
                               form=edit_order_form,
                               result_url=url_for('edit_order', item_id=item_id))


@app.route('/show_orders')
def show_orders():
    items_on_page, page_number = _get_page_items_and_count(request)
    if (not current_user.is_anonymous) and current_user.is_admin:
        orders = Order.query.filter().all()
        t_headers = [('created', None), ('status', None), ('user', None), ('_X_ADMIN_UPDATE', None)]#, ('_X_ADMIN_DELETE', None)]
        t_rows = [[{'icon': None, 'text': order.created, 'url': url_for('order_page', item_id=order.id), },
                   {'icon': None, 'text': get_status_text(order.status), 'url': None, },
                   {'icon': None, 'text': (lambda x: x.firstname + ' ' + x.lastname)(Users.query.filter(Users.id == order.user_id).all()[0]), 'url': url_for('edit_user', item_id=order.user_id)},
                   {'icon': 'glyphicon glyphicon-cog', 'text': None, 'url': url_for('edit_order', item_id=order.id)}] for order in orders]#,
                   #{'icon': 'glyphicon glyphicon-remove', 'text': None, 'url': 'logout', }] for order in orders]
    else:
        orders = Order.query.filter(Order.user_id == current_user.id).all()
        t_headers = [('created', None), ('status', None), ]
        t_rows = [[{'icon': None, 'text': order.created, 'url': url_for('order_page', item_id=order.id), },
                   {'icon': None, 'text': get_status_text(order.status), 'url': None, },] for order in orders]
    return render_template('table_template.html',
                           pg_items_opts=[{'count': i, 'selected': True if i == items_on_page else False} for i in PAGINATION_SIZES],
                           pg_number=page_number,
                           count_of_pages=ceil(len(orders) / items_on_page),
                           items_on_page=items_on_page,
                           result_route='show_users',
                           title='show users',
                           table_title='Users',
                           table_headers=t_headers,
                           table_rows=t_rows)


@app.route('/order_page')
def order_page():
    if current_user.is_anonymous:
        return render_template('msg_template.html',
                               title='no access', msg='Your have no access for looking this page=(')
    items_on_page, page_number = _get_page_items_and_count(request)
    order_id, current_order = _get_item_id_and_item(request, Order)
    if order_id == -1:
        return render_template('msg_template.html',
                               title='bad order id', msg='id {} is not correct'.format(request.args.get('item_id')))
    elif order_id == -2:
        return render_template('msg_template.html',
                               title='bad order id', msg='order  with id {} is not found'.format(request.args.get('item_id')))
    items_in_order = ItemsInOrder.query.filter(ItemsInOrder.order_id == current_order.id).all()
    order_address = Addresses.query.filter(Addresses.id == current_order.address_id).all()[0]
    if current_user.is_admin:
        order_user = (lambda x: x.firstname + ' ' + x.lastname)(Users.query.filter(Users.id == current_order.user_id).all()[0])
    else:
        order_user = current_user.firstname + ' ' + current_user.lastname
    t_headers = [('item', None), ('count', None), ('cost', None), ]
    t_rows = []
    for order_item in items_in_order:
        cur_item_ = Items.query.filter(Items.id == order_item.item_id).all()[0]
        t_rows.append([{'icon': None, 'text': cur_item_.title, 'url': None, },
                       {'icon': None, 'text': order_item.item_count, 'url': None, },
                       {'icon': None, 'text': cur_item_.cost * order_item.item_count, 'url': None, }, ])
    return render_template('order_page.html',
                           pg_items_opts=[{'count': i, 'selected': True if i == items_on_page else False} for i in PAGINATION_SIZES],
                           pg_number=page_number,
                           count_of_pages=ceil(len(items_in_order) / items_on_page),
                           items_on_page=items_on_page,
                           item_id=order_id,
                           result_route='order_page',
                           title='Order',
                           table_title='Order',
                           table_headers=t_headers,
                           table_rows=t_rows,
                           order_user=order_user,
                           created=current_order.created,
                           deadline=current_order.deadline,
                           status=get_status_text(current_order.status),
                           address='{} {} {} {} {} {}'.format(order_address.country,
                                                              order_address.region,
                                                              order_address.city,
                                                              order_address.street,
                                                              order_address.house,
                                                              order_address.apart),
                           delivery_cost=current_order.delivery_cost,
                           pay_meth=get_pay_meth_text(current_order.pay_meth),
                           comment=current_order.comment)


@app.route('/add_to_favorites')
def add_to_favorites():
    item_id, current_item = _get_item_id_and_item(request, Items)
    if item_id == -1:
        return render_template('msg_template.html',
                               title='bad item id', msg='id {} is not correct'.format(request.args.get('item_id')))
    elif item_id == -2:
        return render_template('msg_template.html',
                               title='bad item id',
                               msg='item  with id {} is not found'.format(request.args.get('item_id')))
    item_in_fv = Favorites(user_id=current_user.id,
                           item_id=current_item.id)
    db.session.add(item_in_fv)
    db.session.commit()
    return render_template('msg_template.html',
                           title='item mark as favorite', msg='Item {} mark as favorite'.format(current_item.title))


@app.route('/delete_from_favorites')
def delete_from_favorites():
    item_id, current_item = _get_item_id_and_item(request, Items)
    if item_id == -1:
        return render_template('msg_template.html',
                               title='bad item id', msg='id {} is not correct'.format(request.args.get('item_id')))
    elif item_id == -2:
        return render_template('msg_template.html',
                               title='bad item id',
                               msg='item  with id {} is not found'.format(request.args.get('item_id')))
    item_in_fv = Favorites.query.filter(and_(Favorites.user_id == current_user.id, Favorites.item_id == item_id)).all()[0]
    db.session.delete(item_in_fv)
    db.session.commit()
    return render_template('msg_template.html',
                           title='item unmark as favorite', msg='Item {} unmark as favorite'.format(current_item.title))


@app.route('/delete_address')
def delete_address():
    adr_id, current_adr = _get_item_id_and_item(request, Addresses)
    if adr_id == -1:
        return render_template('msg_template.html',
                               title='bad address id', msg='id {} is not correct'.format(request.args.get('item_id')))
    elif adr_id == -2:
        return render_template('msg_template.html',
                               title='bad address id', msg='address  with id {} is not found'.format(request.args.get('item_id')))
    db.session.delete(current_adr)
    db.session.commit()
    return render_template('msg_template.html',
                           title='address deleted', msg='Address deleted successfully')


@app.route('/add_item', methods = ['GET', 'POST'])
def add_item():
    add_item_form = AddItemForm()
    if add_item_form.validate_on_submit():
        Item = Items(title=add_item_form.title.data,
                     remain_count=add_item_form.remain_count.data,
                     cost=add_item_form.cost.data,
                     description=add_item_form.description.data)
        db.session.add(Item)
        db.session.commit()
        return render_template('msg_template.html',
                               title='item added', msg='Item {} added successfully'.format(add_item_form.title.data))
    else:
        return render_template('add_item_form.html',
                               form=add_item_form)


@app.route('/edit_item', methods = ['GET', 'POST'])
def edit_item():
    item_id, current_item = _get_item_id_and_item(request, Items)
    if item_id == -1:
        return render_template('msg_template.html',
                               title='bad item id', msg='id {} is not correct'.format(request.args.get('item_id')))
    elif item_id == -2:
        return render_template('msg_template.html',
                               title='bad item id', msg='item  with id {} is not found'.format(request.args.get('item_id')))
    edit_item_form = AddItemForm()
    if edit_item_form.validate_on_submit():
        current_item.title = edit_item_form.title.data
        current_item.remain_count = edit_item_form.remain_count.data
        current_item.cost = edit_item_form.cost.data
        current_item.description = edit_item_form.description.data
        db.session.commit()
        return render_template('msg_template.html',
                               title='item updated', msg='Item {} updated successfully'.format(edit_item_form.title.data))
    else:
        edit_item_form.title.data = current_item.title
        edit_item_form.remain_count.data = current_item.remain_count
        edit_item_form.cost.data = current_item.cost
        edit_item_form.description.data = current_item.description
        return render_template('edit_item_form.html',
                               form=edit_item_form,
                               result_url=url_for('edit_item', item_id=item_id))


@app.route('/delete_item')
def delete_item():
    item_id, current_item = _get_item_id_and_item(request, Items)
    if item_id == -1:
        return render_template('msg_template.html',
                               title='bad item id', msg='id {} is not correct'.format(request.args.get('item_id')))
    elif item_id == -2:
        return render_template('msg_template.html',
                               title='bad item id', msg='item  with id {} is not found'.format(request.args.get('item_id')))
    title = current_item.title
    # we can not delete item if it used in backets or orders
    associated_backets = Basket.query.filter(Basket.item_id == current_item.id).all()
    if associated_backets:
        return render_template('msg_template.html',
                               title='can not be deleted!', msg='Item {} can not be deleted because it used in some backets!'.format(title))
    associated_orders = ItemsInOrder.query.filter(ItemsInOrder.item_id == current_item.id).all()
    if associated_orders:
        return render_template('msg_template.html',
                               title='can not be deleted!',
                               msg='Item {} can not be deleted because it used in some orders!'.format(title))
    # delete associated from favorites and from feedbacks
    for item_ in Favorites.query.filter(Favorites.item_id == current_item.id).all():
        db.session.delete(item_)
    for item_ in Feedbacks.query.filter(Feedbacks.item_id == current_item.id).all():
        db.session.delete(item_)
    db.session.delete(current_item)
    db.session.commit()
    return render_template('msg_template.html',
                           title='item deleted', msg='Item {} deleted successfully'.format(title))


@app.route('/remove_from_basket')
def remove_from_basket():
    try:
        item_id = int(request.args.get('item_id'))
        if item_id < 1:
            raise ValueError
    except (ValueError, TypeError):
        return render_template('msg_template.html',
                               title='bad item id', msg='id {} is not correct'.format(request.args.get('item_id')))
    try:
        current_basket_item = Basket.query.filter(and_(Basket.user_id == current_user.id, Basket.item_id == item_id)).all()[0]
        current_item = Items.query.filter(Items.id == current_basket_item.item_id).all()[0]
    except IndexError:
        return render_template('msg_template.html',
                               title='bad item id', msg='item  with id {} is not found'.format(request.args.get('item_id')))
    current_item.remain_count += current_basket_item.item_count
    db.session.delete(current_basket_item)
    db.session.commit()
    return render_template('msg_template.html',
                           title='item deleted', msg='Item {} deleted successfully'.format(current_item.title))


@app.route('/item_page', methods = ['GET', 'POST'])
def item_page():
    item_id, current_item = _get_item_id_and_item(request, Items)
    if item_id == -1:
        return render_template('msg_template.html',
                               title='bad item id', msg='id {} is not correct'.format(request.args.get('item_id')))
    elif item_id == -2:
        return render_template('msg_template.html',
                               title='bad item id', msg='item  with id {} is not found'.format(request.args.get('item_id')))
    add_item_to_basket_form = AddToBasketForm(current_item)
    if current_user.is_anonymous:
        return render_template('item_page.html',
                               item=current_item,
                               item_id=item_id,
                               item_in_basket=False,
                               form=add_item_to_basket_form)
    if request.method == 'POST':
        if add_item_to_basket_form.add_to_basket.data:
            if add_item_to_basket_form.validate():
                db.session.add(Basket(user_id=current_user.id,
                                  item_id=current_item.id,
                                  item_count=add_item_to_basket_form.count.data,
                                  order_date=datetime.now()))
                current_item.remain_count -= add_item_to_basket_form.count.data
                db.session.commit()
                item_in_basket = True
            else:
                item_in_basket = False
        elif add_item_to_basket_form.remove_from_basket.data:
            current_basket_item = Basket.query.filter(and_(Basket.user_id == current_user.id, Basket.item_id == current_item.id)).all()[0]
            current_item.remain_count += current_basket_item.item_count
            db.session.delete(current_basket_item)
            db.session.commit()
            item_in_basket = False
        else:
            # TODO POST via add_item_to_basket_form and ни одна кнопка не нажата, wtf?
            return render_template('msg_template.html', title='something bad!', msg='something shit was happened')
    else:
        item_in_basket = True if len(Basket.query.filter(and_(Basket.user_id == current_user.id, Basket.item_id == current_item.id)).all()) > 0 else False
    return render_template('item_page.html',
                           item=current_item,
                           item_id=item_id,
                           item_in_basket=item_in_basket,
                           form=add_item_to_basket_form)


@app.route('/show_items', methods = ['GET', 'POST'])
def show_items():
    items_on_page, page_number = _get_page_items_and_count(request)
    cost_min_real = db.session.query(func.min(Items.cost)).scalar()
    cost_max_real = db.session.query(func.max(Items.cost)).scalar()
    try:
        cost_min = int(request.form.get('cost_from'))
        if cost_min < 0:
            raise ValueError
    except (ValueError, TypeError):
        cost_min = cost_min_real
    try:
        cost_max = int(request.form.get('cost_to'))
        if cost_max < 0:
            raise ValueError
    except (ValueError, TypeError):
        cost_max = cost_max_real
    t_headers = [('title', 'width=20%'), ('count', 'width=10%'), ('cost', 'width=10%'), ('description', None)]
    current_items = Items.query.filter(and_(Items.cost >= cost_min, Items.cost <= cost_max)).limit(items_on_page).offset(items_on_page * (page_number-1)).all()
    t_rows = [[{'icon': None, 'text': item.title, 'url': url_for('item_page', item_id=item.id),},
               {'icon': None if item.remain_count > 10 else 'glyphicon glyphicon-exclamation-sign', 'text': item.remain_count, 'url': None,},
               {'icon': None, 'text': item.cost, 'url': None,},
               {'icon': None, 'text': item.description, 'url': None,}] for item in current_items]
    if (not current_user.is_anonymous):
        t_headers.append(('favorites', None))
        user_favorites = Favorites.query.filter(Favorites.user_id == current_user.id).all()  # all favorites for this user
        favorites_ids = [x.item_id for x in user_favorites]
        for i, t_row in enumerate(t_rows):
                t_row.append({
                    'icon': 'glyphicon glyphicon-heart' if current_items[i].id in favorites_ids else 'glyphicon glyphicon-heart-empty',
                    'text': None,
                    'url': url_for('delete_from_favorites', item_id=current_items[i].id) if current_items[i].id in favorites_ids else url_for('add_to_favorites', item_id=current_items[i].id)})
        if current_user.is_admin:
            t_headers.append(('_X_ADMIN_UPDATE', None))
            t_headers.append(('_X_ADMIN_DELETE', None))
            for i, t_row in enumerate(t_rows):
                t_row.append({'icon': 'glyphicon glyphicon-cog', 'text': None, 'url': url_for('edit_item', item_id=current_items[i].id)})
                t_row.append({'icon': 'glyphicon glyphicon-remove', 'text': None, 'url': url_for('delete_item', item_id=current_items[i].id) })
    return render_template('items_table.html',
                           pg_items_opts=[{'count': i, 'selected': True if i == items_on_page else False} for i in PAGINATION_SIZES],
                           pg_number=page_number,
                           count_of_pages=ceil(db.session.query(func.count(Items.id)).scalar() / items_on_page),
                           items_on_page=items_on_page,
                           title='show items',
                           table_title='Items',
                           table_headers=t_headers,
                           table_rows=t_rows,
                           cost_min=cost_min,
                           cost_max=cost_max)


@app.route('/show_users')
def show_users():
    items_on_page, page_number = _get_page_items_and_count(request)
    t_headers = [('login', None), ('admin', None), ('name', None), ('email', None), ('phone', None), ('last session', None)]
    current_users = Users.query.limit(items_on_page).offset(items_on_page * (page_number-1)).all()
    t_rows = [[{'icon': None, 'text': user.login, 'url': None,},
               {'icon': 'glyphicon glyphicon-ok-circle' if user.is_admin else 'glyphicon glyphicon-remove-circle', 'text': None, 'url': None, },
               {'icon': None, 'text': user.firstname + ' ' + user.lastname, 'url': None,},
               {'icon': None, 'text': user.email, 'url': None,},
               {'icon': None, 'text': user.phone, 'url': None, },
               {'icon': None, 'text': user.last_sess, 'url': None,}] for user in current_users]
    if (not current_user.is_anonymous) and current_user.is_admin:
        t_headers.append(('_X_ADMIN_UPDATE', None))
        t_headers.append(('_X_ADMIN_DELETE', None))
        for i, t_row in enumerate(t_rows):
            t_row.append({'icon': 'glyphicon glyphicon-cog', 'text': None, 'url': url_for('edit_user', item_id=current_users[i].id)})
            t_row.append({'icon': 'glyphicon glyphicon-remove', 'text': None, 'url': url_for('delete_user', item_id=current_users[i].id), })
    return render_template('table_template.html',
                           pg_items_opts=[{'count': i, 'selected': True if i == items_on_page else False} for i in PAGINATION_SIZES],
                           pg_number=page_number,
                           count_of_pages=ceil(db.session.query(func.count(Users.id)).scalar() / items_on_page),
                           items_on_page=items_on_page,
                           result_route='show_users',
                           title='show users',
                           table_title='Users',
                           table_headers=t_headers,
                           table_rows=t_rows)


@app.route('/import_item', methods=['GET', 'POST'])
def import_item():
    import_item_form = ImportItemForm()
    if import_item_form.validate_on_submit():
        # TODO now it is very bad code
        for row in request.files['file'].stream.readlines()[1:]:
            try:
                row_spl = row.decode('utf-8').split(';')
                db.session.add(Items(title=row_spl[0].replace('\n', ''),
                                     remain_count=int(row_spl[1]),
                                     cost=float(row_spl[2]),
                                     description=row_spl[3].replace('\n', '')))
            except (IndexError, ValueError):
                continue
        db.session.commit()
        return render_template('msg_template.html',
                               title='file imported',
                               msg='File {} imported successfully'.format(import_item_form.file.data))
    else:
        return render_template('import_item_form.html',
                               form=import_item_form,)


@app.route('/basket', methods = ['GET', 'POST'])
def basket():
    items_on_page, page_number = _get_page_items_and_count(request)
    t_headers = [('item', None), ('count', 'width=10%'), ('summary cost', 'width=20%'), ('remove', 'width=10%')]
    current_basket_items = Basket.query.filter(Basket.user_id == current_user.id).limit(items_on_page).offset(items_on_page * (page_number-1)).all()
    t_rows = [[{'icon': None, 'text': Items.query.filter(Items.id == item.item_id).all()[0].title, 'url': url_for('item_page', item_id=item.item_id),},
               {'icon': None, 'text': item.item_count, 'url': None,},
               {'icon': None, 'text': Items.query.filter(Items.id == item.item_id).all()[0].cost * item.item_count, 'url': None,},
               {'icon': 'glyphicon glyphicon-remove', 'text': None, 'url': url_for('remove_from_basket', item_id=item.item_id),}] for item in current_basket_items]
    return render_template('basket_template.html',
                           pg_items_opts=[{'count': i, 'selected': True if i == items_on_page else False} for i in PAGINATION_SIZES],
                           pg_number=page_number,
                           count_of_pages=ceil(len(current_basket_items) / items_on_page),
                           items_on_page=items_on_page,
                           title='Basket',
                           table_title='Basket',
                           table_headers=t_headers,
                           table_rows=t_rows,
                           summary_cost=sum([Items.query.filter(Items.id == item.item_id).all()[0].cost * item.item_count for item in current_basket_items]))
