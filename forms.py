from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, SubmitField, ValidationError, BooleanField, IntegerField, \
    PasswordField, FloatField, FileField, SelectField
from wtforms.validators import DataRequired, Length, Email


from mg52_webapp_flask import db
from mg52_webapp_flask.models import Users, Addresses


def validate_phone(form, phone):
    if not phone.data.isdigit():
        raise ValidationError('phone must be numeric')

def validate_item_count(form, remain_count):
    if not remain_count.data > -1:
        raise ValidationError('count must be non-negative')

def validate_item_cost(form, cost):
    if not float(cost.data) >= 0.0:
        raise ValidationError('cost must be non-negative')

def validate_confirm_pass(form, confirm_pass):
    if confirm_pass.data != form.password.data:
        raise ValidationError('passwords are not equal')

def validate_unigue_login(form, login):
    if login.data in [user.login for user in db.session.query(Users).all()]:
        raise ValidationError('this login is already used')

def validate_count_to_basket(form, count):
    if int(count.data) < 0:
        raise ValidationError('count must be non-negative')
    if int(count.data) > form.item.remain_count:
        raise ValidationError('count must be less or equal to remain count of item')


class AddUserForm(FlaskForm):
    login = StringField('Login', [DataRequired(), Length(max=24, message='login > 24 symbols')], render_kw={'class': 'form-control'})
    is_admin = BooleanField('Admin access')
    password = PasswordField('Password', [DataRequired(), Length(min=4, max=240, message='password < 4 symbols or > 240')], render_kw={'class': 'form-control'})
    firstname = StringField('Firstname', [DataRequired(), Length(max=24, message='firstname > 24 symbols')], render_kw={'class': 'form-control'})
    lastname = StringField('Lastname', [DataRequired(), Length(max=24, message='lastname > 24 symbols')], render_kw={'class': 'form-control'})
    email = StringField('Email', [DataRequired(), Email(message='email is incorrect')], render_kw={'class': 'form-control'})
    phone = StringField('Phone', [DataRequired(), Length(max=24, message='phone > 24 symbols'), validate_phone], render_kw={'class': 'form-control'})
    submit = SubmitField('Submit changes', render_kw={'class': 'btn btn-primary'})


class EditUserForm(FlaskForm):
    is_admin = BooleanField('Admin access')
    firstname = StringField('Firstname', [DataRequired(), Length(max=24, message='firstname > 24 symbols')], render_kw={'class': 'form-control'})
    lastname = StringField('Lastname', [DataRequired(), Length(max=24, message='lastname > 24 symbols')], render_kw={'class': 'form-control'})
    email = StringField('Email', [DataRequired(), Email(message='email is incorrect')], render_kw={'class': 'form-control'})
    phone = StringField('Phone', [DataRequired(), Length(max=24, message='phone > 24 symbols'), validate_phone], render_kw={'class': 'form-control'})
    submit = SubmitField('Submit changes', render_kw={'class': 'btn btn-primary'})


class RegistrationForm(FlaskForm):
    login = StringField('Login', [DataRequired(), Length(max=24, message='login > 24 symbols'), validate_unigue_login], render_kw={'class': 'form-control'})
    password = PasswordField('Password', [DataRequired(), Length(min=4, max=240, message='password < 24 symbols of > 240')], render_kw={'class': 'form-control'})
    confirm_pass = PasswordField('Confirm password', [DataRequired(), validate_confirm_pass], render_kw={'class': 'form-control'})
    firstname = StringField('Firstname', [DataRequired(), Length(max=24, message='firstname > 24 symbols')], render_kw={'class': 'form-control'})
    lastname = StringField('Lastname', [DataRequired(), Length(max=24, message='lastname > 24 symbols')], render_kw={'class': 'form-control'})
    email = StringField('Email', [DataRequired(), Email(message='email is incorrect')], render_kw={'class': 'form-control'})
    phone = StringField('Phone', [DataRequired(), Length(max=24, message='phone > 24 symbols'), validate_phone], render_kw={'class': 'form-control'})
    submit = SubmitField('Register', render_kw={'class': 'btn btn-primary'})


class AddItemForm(FlaskForm):
    title = StringField('Title', [DataRequired(), Length(max=128, message='title > 128 symbols')], render_kw={'class': 'form-control'})
    remain_count = IntegerField('Count', [DataRequired(message='Count is required and must be integer'), validate_item_count], render_kw={'class': 'form-control'})
    cost = FloatField('Cost', [DataRequired(), validate_item_cost], render_kw={'class': 'form-control'})
    description = TextAreaField('Description', [DataRequired(), Length(max=1024, message='description > 1024 symbols')], render_kw={'class': 'form-control'})
    submit = SubmitField('Submit changes', render_kw={'class': 'btn btn-primary'})


class LoginForm(FlaskForm):
    login = StringField('Login', validators=[DataRequired()], render_kw={'class': 'form-control'})
    password = PasswordField('Password', validators=[DataRequired()], render_kw={'class': 'form-control'})
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In', render_kw={'class': 'btn btn-primary'})


class ImportItemForm(FlaskForm):
    file = FileField('CSV file', validators=[DataRequired()])
    submit = SubmitField('Upload')


class AddToBasketForm(FlaskForm):

    def __init__(self, item_to_add):
        super().__init__()
        self.item = item_to_add

    add_to_basket = SubmitField('Add to basket', render_kw={'class': 'btn btn-primary'})
    count = IntegerField('Count', [DataRequired(message='Count is required and must be integer'), validate_count_to_basket], default=1, render_kw={'class': 'form-control', 'placeholder': 'count'})
    remove_from_basket = SubmitField('Remove from basket', render_kw={'class': 'btn btn-info'})


class AddAddressForm(FlaskForm):
    country = StringField('Country', validators=[DataRequired()], render_kw={'class': 'form-control'})
    region = StringField('Region', validators=[DataRequired()], render_kw={'class': 'form-control'})
    city = StringField('City', validators=[DataRequired()], render_kw={'class': 'form-control'})
    street = StringField('Street', validators=[DataRequired()], render_kw={'class': 'form-control'})
    house = StringField('House', validators=[DataRequired()], render_kw={'class': 'form-control'})
    apart = StringField('Apart', validators=[DataRequired()], render_kw={'class': 'form-control'})
    submit = SubmitField('Submit', render_kw={'class': 'btn btn-primary'})


class CreateOrderForm(FlaskForm):
    address = SelectField('Address', render_kw={'class': 'form-control'})
    delivery_date = SelectField('Delivery date', render_kw={'class': 'form-control'})
    payment_method = SelectField('Payment method', render_kw={'class': 'form-control'})
    comment = TextAreaField('Description', [Length(max=2048, message='comment > 2048 symbols')], render_kw={'class': 'form-control'})
    submit = SubmitField('Create order', render_kw={'class': 'btn btn-primary'})


class EditOrderForm(FlaskForm):
    status = SelectField('Select new order status', render_kw={'class': 'form-control'})
    submit = SubmitField('Submit', render_kw={'class': 'btn btn-primary'})


class AddFeedbackForm(FlaskForm):
    feedback = TextAreaField('Feedback', [Length(max=2048, message='comment > 2048 symbols')], render_kw={'class': 'form-control'})
    submit = SubmitField('Submit', render_kw={'class': 'btn btn-primary'})
