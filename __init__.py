from os import environ

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager


class Config:
    FLASK_DEBUG = environ.get(
        'FLASK_DEBUG', '1'
    )
    SQLALCHEMY_DATABASE_URI = environ.get(
        'SQLALCHEMY_DATABASE_URI',
        'top_secret'
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = environ.get(
        'SQLALCHEMY_TRACK_MODIFICATIONS', True
    )
    SQLALCHEMY_ECHO = environ.get(
        'SQLALCHEMY_ECHO', True
    )

    SECRET_KEY = 'CSRF-secret-key'


app = Flask(__name__, static_folder='static', template_folder='templates')
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
login = LoginManager(app)
login.login_view = 'login'

from mg52_webapp_flask import routes
from mg52_webapp_flask import models

#db.create_all()  # создаем таблицы автоматически