from mg52_webapp_flask import db, login
from werkzeug.security import generate_password_hash, check_password_hash
from datetime import datetime
from flask_login import UserMixin


class Users(UserMixin, db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(64), index=True, unique=True, nullable=False)
    password_hash = db.Column(db.String(128), nullable=False)
    firstname = db.Column(db.String(64), index=True, nullable=False)
    lastname = db.Column(db.String(64), index=True)
    email = db.Column(db.String(64), index=True, unique=True, nullable=False)
    phone = db.Column(db.String(16), index=True, nullable=False)
    last_sess = db.Column(db.DateTime, nullable=False)
    basket = db.relationship("Basket")
    favorites = db.relationship("Favorites")
    bonus_card = db.relationship("BonusCards", uselist=False, backref='users')
    is_admin = db.Column(db.Boolean, nullable=False)
    feedbacks = db.relationship("Feedbacks")

    def __repr__(self):
        return 'User {} ({} {})'.format(self.login, self.firstname, self.lastname)

    def set_pass(self, plain_pass):
        self.password_hash = generate_password_hash(plain_pass)

    def check_pass(self, plain_pass):
        return check_password_hash(self.password_hash, plain_pass)


class Basket(db.Model):
    __tablename__ = 'basket'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    item_id = db.Column(db.Integer, db.ForeignKey('items.id'), nullable=False)
    item_count = db.Column(db.Integer, nullable=False)
    order_date = db.Column(db.DateTime, nullable=False)

    def __repr__(self):
        return 'Basket for user {}'.format(self.user_id)


class Favorites(db.Model):
    __tablename__ = 'favorites'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    item_id = db.Column(db.Integer, db.ForeignKey('items.id'), nullable=False)

    def __repr__(self):
        return 'Favorites for user {}'.format(self.user_id)


class BonusCards(db.Model):
    __tablename__ = 'bonus_cards'
    id = db.Column(db.Integer, primary_key=True)
    card_number = db.Column(db.String(20), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    balance = db.Column(db.Integer, nullable=False)
    is_blocked = db.Column(db.Boolean, nullable=False)

    def __repr__(self):
        return 'Bonus card for user {}'.format(self.user_id)


class Items(db.Model):
    __tablename__ = 'items'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(128), nullable=False)
    remain_count = db.Column(db.Integer, nullable=False)
    cost = db.Column(db.Float, nullable=False)
    description = db.Column(db.String(2048))
    basket = db.relationship("Basket")
    favorites = db.relationship("Favorites")
    feedbacks = db.relationship("Feedbacks")

    #def __repr__(self):
    #    return [self.title, self.remain_count, self.cost, self.description]


class Feedbacks(db.Model):
    __tablename__ = 'feedbacks'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=True)  # if deleted after
    item_id = db.Column(db.Integer, db.ForeignKey('items.id'), nullable=False)
    feedback = db.Column(db.String(2048))


class Order(db.Model):
    __tablename__ = 'order'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, nullable=False)
    status = db.Column(db.Integer, nullable=False)
    assigned_to = db.Column(db.Integer, nullable=False)  # id from users
    created = db.Column(db.DateTime, nullable=False)
    deadline = db.Column(db.DateTime, nullable=False)
    address_id = db.Column(db.Integer, db.ForeignKey('addresses.id'), nullable=False)
    delivery_cost = db.Column(db.Integer, nullable=False)
    items_in_order = db.relationship("ItemsInOrder", uselist=False, backref='order')
    comment = db.Column(db.String(2048))
    pay_meth = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        return 'order from user {}'.format(self.user_id)

class ItemsInOrder(db.Model):
    __tablename__ = 'items_in_order'
    id = db.Column(db.Integer, primary_key=True)
    order_id = db.Column(db.Integer, db.ForeignKey('order.id'), nullable=False)
    item_id = db.Column(db.Integer, db.ForeignKey('items.id'), nullable=False)
    item_count = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        return 'Items for order {}'.format(self.order_id)


class Addresses(db.Model):
    __tablename__ = 'addresses'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, nullable=False)
    country = db.Column(db.String(128))
    region = db.Column(db.String(128))
    city = db.Column(db.String(128))
    street = db.Column(db.String(128))
    house = db.Column(db.String(128))
    apart = db.Column(db.String(128))
    orders = db.relationship("Order")

    def __repr__(self):
        return 'employer {} {}'.format(self.firstname, self.lastname)


@login.user_loader
def load_user(id):
    return Users.query.get(int(id))